#!/bin/bash
yum update -y
yum install -y httpd24 php71 php71-iconv php71-mbstring php71-curl php71-openssl php71-tokenizer php71-xmlrpc php71-soap php71-ctype php71-zip php71-gd php71-simplexml php71-spl php71-pcre php71-dom php71-xml php71-intl php71-json php71-mysqli; yum clean all
mkdir /var/www/html/pyxis
chown apache.apache /var/www/html/pyxis
chown apache.apache /home/ec2-user/pyxis.conf
cp -p /home/ec2-user/pyxis.conf /etc/httpd/conf.d/
sed -i s/DocumentRoot/#DocumentRoot/g /etc/httpd/conf/httpd.conf
echo "<html><body><h1>Hello Pyxis!</h1></body><html>" > /var/www/html/pyxis/index.php
chkconfig httpd on
service httpd start
